using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
    public class Haffman
    {
        // закодированные симолы
        internal static Dictionary<char, string> CodedChars = new Dictionary<char, string>();

        // словарь частот
        internal static Dictionary<char, int> FrequencyDictionary = new Dictionary<char, int>();

        // узел хафмана
        private struct HuffmannNode
        {
            public string text;

            public int frequency;

            public HuffmannNode(string t, int f)
            {
                text = t;
                frequency = f;
            }
        };

        // все симводы
        private static string _chars;

        // список узлов который сортируется по частоте и два последних узла сливаются
        private static List<HuffmannNode> _huffmannNodes = new List<HuffmannNode>();

        private static void SortList()
        {
            for (var i = 0; i < _huffmannNodes.Count - 1; i++)
            {
                for (var j = i; j < _huffmannNodes.Count; j++)
                {
                    if (_huffmannNodes[i].frequency >= _huffmannNodes[j].frequency)
                        continue;

                    var buf = _huffmannNodes[i];
                    _huffmannNodes[i] = _huffmannNodes[j];
                    _huffmannNodes[j] = buf;
                }
            }
        }

        public static string DoСompression(string text0)
        {
            // закодированные симолы
            Dictionary<char, string> codedChars = new Dictionary<char, string>();
            List<HuffmannNode> huffmannNodes = new List<HuffmannNode>();

            var text = text0;
            var result = "";

            //Считаем частоту
            foreach (var t in text)
            {
                if (FrequencyDictionary.Keys.Contains(t))
                {
                    FrequencyDictionary[t]++;
                }
                else
                {
                    FrequencyDictionary.Add(t, 1);
                }
            }

            //Начальное заполнение данных
            foreach (var i in FrequencyDictionary)
            {
                codedChars.Add(i.Key, "");
                huffmannNodes.Add(new HuffmannNode(i.Key.ToString(), i.Value));
                _chars += i.Key;
            }

            //Кодируем символы
            while (huffmannNodes.Count > 1)
            {
                _huffmannNodes = huffmannNodes;
                SortList();

                foreach (var i in _chars)
                {
                    if (huffmannNodes[huffmannNodes.Count - 2].text.Contains(i))
                    {
                        codedChars[i] = "0" + codedChars[i];
                    }
                    else if (huffmannNodes[huffmannNodes.Count - 1].text.Contains(i))
                    {
                        codedChars[i] = "1" + codedChars[i];
                    }
                }

                huffmannNodes[huffmannNodes.Count - 2] = new HuffmannNode(
                    huffmannNodes[huffmannNodes.Count - 2].text + huffmannNodes[huffmannNodes.Count - 1].text,
                    huffmannNodes[huffmannNodes.Count - 2].frequency +
                    huffmannNodes[huffmannNodes.Count - 1].frequency);

                huffmannNodes.RemoveAt(huffmannNodes.Count - 1);
            }

            //Из закодированных символов получаем текст
            foreach (var i in text)
            {
                foreach (var htn in codedChars)
                {
                    if (htn.Key == i)
                    {
                        result += htn.Value;
                    }
                }
            }

            CodedChars = codedChars;

            return result;
        }

        public static double CulcH(string str)
        {
            double result = 0;

            foreach (var i in _chars)
            {
                // H = разница всех  LOG(частота в долях, 2) * частота в долях 
                result -= (float) FrequencyDictionary[i] / str.Length
                          * Math.Log((float) FrequencyDictionary[i] / str.Length, 2.0f);
            }

            return result;
        }

        public static float CulcLength(string str)
        {
            float result = 0;

            foreach (var i in _chars)
            {
                // L = сумма всех {длинна кодоого слова} * частоту в долях 
                result += CodedChars[i].Length * (float) FrequencyDictionary[i] / str.Length;
            }

            return result;
        }
    }
}