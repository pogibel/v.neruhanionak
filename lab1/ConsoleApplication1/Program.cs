﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //  var str = ReadFile("D:\\3курс\\ТИ\\немецкий.txt");

            var str = "ананас";
            
            var сompressedStr = Сompression(str);

            //ToFile(сompressedStr, "D:\\3курс\\ТИ\\file.txt");

            var L = Haffman.CulcLength(str);
            var H = Haffman.CulcH(str);

            Console.WriteLine("L = " + L);
            Console.WriteLine("H = " + H);
            Console.WriteLine("избыточность кода = " + (L - H));

            Console.WriteLine("проверка теоремы 2 (H < L < H + 1/L) : " + H + " < " + L + " < " + (H + 1.0f / L));
        }

        private static string Сompression(string str)
        {
            Console.WriteLine(str + " " + str.Length);
            Console.WriteLine(GetBytes(str));

            var сompressedStr = Haffman.DoСompression(str);

            Console.WriteLine(сompressedStr);

            foreach (var i in Haffman.CodedChars.OrderBy(pair => pair.Value.Length))
            {
                Console.WriteLine("char : " + i.Key + " : " + i.Value);
            }

            foreach (var i in Haffman.FrequencyDictionary.OrderByDescending(pair => pair.Value))
            {
                Console.WriteLine("char : " + i.Key + " : " + i.Value + " : " +
                                  (float) Haffman.FrequencyDictionary[i.Key] / str.Length);
            }

            Console.WriteLine(" befor : " + GetBytes(str).Length + "\n after : " + сompressedStr.Length);

            return сompressedStr;
        }

        private static string ReadFile(string filePath)
        {
            var lines = File.ReadAllLines(filePath);

            var str = "";
            foreach (var s in lines)
            {
                str += s;
            }

            return str;
        }

        private static void ToFile(string strstr, string filePath)
        {
            var bitArr = new BitArray(strstr.Length);

            var j = 0;

            foreach (var i in strstr)
            {
                if (i == '1')
                {
                    bitArr[j] = true;
                }
                else
                {
                    bitArr[j] = false;
                }

                j++;
            }

            Stream stream = new FileStream(filePath, FileMode.Create);
            var sw = new StreamWriter(stream);

            foreach (bool bit in bitArr)
            {
                sw.Write(bit ? 1 : 0);
            }

            sw.Flush();
            sw.Close();
        }

        private static string GetBytes(string data)
        {
            var textAsBytes = "";
            var btr = new BitArray(Encoding.ASCII.GetBytes(data));

            for (var index = 0; index < btr.Count; index++)
            {
                textAsBytes += btr[index] ? "1" : "0";
            }

            return textAsBytes;
        }
    }
}